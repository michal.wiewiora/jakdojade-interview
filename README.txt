Please use the start.sh/start.bat script (linux/windows)

1. To run the tests
------------------------------------
mvnw clean test

2. To build and start the WEB server 
(http://localhost:8080/bike-station)
------------------------------------
mvnw spring-boot:run

-- 3. Available resources
------------------------------------
- GET 	http://localhost:8080/bike-station/{id}
- GET 	http://localhost:8080/bike-station
- GET 	http://localhost:8080/bike-station/{id}/stands
- DELETE 	http://localhost:8080/bike-station/{1}
- POST 	http://localhost:8080/bike-station/{1}
{
 "stationName": "station_name", 
 "allStands": 10
}
- PUT	http://localhost:8080/bike-station/{1}
{
"id": "41d802b1-e3ea-4bcf-9e60-828d919c3f02",
"stationName": "station_new_name"
"allStands": 10
}
- PUT	http://localhost:8080/bike-station/{1}/rent
{
  "bikeId": "059923f9-f1fa-4094-a9ba-eb26e6530ac4",
  "stationId": "7241ef72-aeb8-48ad-bef6-5a1fc237eb0d",
  "standId" : "a504f876-f538-4afb-96ca-e33873b46206"
}
- PUT	http://localhost:8080/bike-station/{1}/return
{
  "bikeId": "059923f9-f1fa-4094-a9ba-eb26e6530ac4",
  "stationId": "7241ef72-aeb8-48ad-bef6-5a1fc237eb0d",
  "standId" : "a504f876-f538-4afb-96ca-e33873b46206"
}

