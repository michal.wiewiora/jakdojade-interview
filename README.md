## General information

Spring boot application exposing the REST API to manage the bike station network.
We have three main model elements: bike-station (`BikeStation`), bike-stand (`BikeStand`) & bike (`Bike`).

Each bike-station contains:
* unique id (UUID)
* name
* set of bike-stands
* set of bikes

Each bike-stand contains:
* unique id (UUID)
* bike (optional)
* parent station id

Each bike is described by:
* unique id (UUID)
* parent station
* parent stand (optional)
* taken flag 

## Allowed actions

System allows to:
* get single bike-station information
* get all bike-stations report (added number of available bikes to the requirements)
* get bike-station stands information
* create a station (name & number of stands)
* delete station
* update the station (name & number of stands) 
* rent the bike
* return the bike

## Main assumptions 

Bikes can be rented (taken) or returned to the station bike-stand or directly to the bike-station.
Number of the bikes in the whole system is constant, that is why we can  only update the station name & number of stands.
Updating the number of stands means, that the stands are removed or added without the bikes (eg. if station has 5 stands,
we can update it and make it has the 10 stands). Bikes from the removed stands are marked as taken. New stands are created without the bikes.

During the start, system creates a default number of stands and stations (initial number of bikes = stands * stations).
There is no persistence layer.

System is designed to be a thread safe (parallel rent, return, update etc. operations should not break the model consistency).

One of the assumptions is that if we are taking or returning the bike, bike-station needs to confirm this action (electric-lock check, or other operation) 
using the external API request / gateway which is stubbed in the system code.

## Endpoints

* `GET /bike-stations/{id}` - get bike station info
* `GET /bike-stations` - get bike stations info
* `DELETE /bike-stations/{id}` - deletes station (marks bikes as taken - they can be returned).
* `POST /bike-stations/{id}` - updates station (name or the number of stands)
  - request format:
		`{
		 "stationName": "station_name", 
		 "allStands": 10
		`}
* `PUT /bike-stations` - adds the bike station with the given name and number of stands (stand are empty)
  - request format:
		`{
			"id": "41d802b1-e3ea-4bcf-9e60-828d919c3f02",
			"stationName": "station_new_name"
			"allStands": 10
		}`
* `PUT /bike-stations/{id}/rent` - rents the bike (from the particular stand or station)
  - request format:
		`{
		  "bikeId": "059923f9-f1fa-4094-a9ba-eb26e6530ac4",
		  "stationId": "7241ef72-aeb8-48ad-bef6-5a1fc237eb0d",
		  "standId" : "a504f876-f538-4afb-96ca-e33873b46206"
		}`
* `PUT /bike-stations/{id}/rent` - returns the bike (to the particular stand or station)
  - request format:
		`{
		  "bikeId": "059923f9-f1fa-4094-a9ba-eb26e6530ac4",
		  "stationId": "7241ef72-aeb8-48ad-bef6-5a1fc237eb0d",
		  "standId" : "a504f876-f538-4afb-96ca-e33873b46206"
		}`

## Testing approach

System contains:	
* unit tests for all model classes (`BikeStation`, `BikeStand`, `Bike`)
* WEB tests for all REST methods (tests matching, input serialization, validatio and response statuses)
* integration & load tests for themain bussiness logic (renting, returning, editing)

Tests can be launched using `mwnw test` command

## Launching the app

`mwnw spring-boot:run`

`http://localhost:8080/bike-stations`

## Final remarks

Good to have in the future (if there would be more time ;-)):
* Securing WEB interfances
* higher REST maturity level (links of allowed metods in each response)