package pl.wiewiora.michal.citybikes.model;

public class BikeStationUpdateException extends BikeStationException {

    public BikeStationUpdateException(String errorMessage) {
        super(errorMessage);
    }
}
