package pl.wiewiora.michal.citybikes.model;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Bike stand not found.")
public class BikeStandNotFoundException extends BikeStationException {

    public BikeStandNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
