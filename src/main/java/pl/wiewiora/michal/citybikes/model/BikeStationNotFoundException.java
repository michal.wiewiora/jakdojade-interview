package pl.wiewiora.michal.citybikes.model;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Bike station not found.")
public class BikeStationNotFoundException extends BikeStationException {

    public BikeStationNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
