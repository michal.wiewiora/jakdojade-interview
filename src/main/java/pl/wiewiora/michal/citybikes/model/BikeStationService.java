package pl.wiewiora.michal.citybikes.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.wiewiora.michal.citybikes.SpringContext;
import pl.wiewiora.michal.citybikes.dto.*;
import pl.wiewiora.michal.citybikes.integration.BikeStationClientException;
import pl.wiewiora.michal.citybikes.integration.BikeStationClientGateway;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class BikeStationService {

    public static final byte DEFAULT_NUMBER_OF_THE_BIKE_STATIONS = 10;

    private static final Logger logger = LoggerFactory.getLogger(BikeStationService.class);

    //all bike stations in the system
    private final Map<String, BikeStation> allStations = Collections.synchronizedMap(new HashMap<>());
    //all bikes in the system (rented and not)
    private final Map<String, Bike> allBikes = Collections.synchronizedMap(new HashMap<>());

    @PostConstruct
    public void init() {
        BikeStation station;
        for(int i=1;i<DEFAULT_NUMBER_OF_THE_BIKE_STATIONS+1;i++) {
            station = new BikeStation("Station_" + i, true);
            station.setBikeStationClientGateway(SpringContext.getBean(BikeStationClientGateway.class));
            allStations.put(station.getId(), station);
            for (BikeStand stand : station.getStands().values()) {
                Bike bike = stand.getParkedBike();
                allBikes.put(bike.getId(), bike);
            }
        }
    }

    /**
     * Returns all stations from the system
     * @return stations
     */
    public Map<String, BikeStation> getAllStations() {
        return allStations;
    }

    /**
     * Returns all bikes from the system
     * @return bikes
     */
    public Map<String, Bike> getAllBikes() {
        return allBikes;
    }

    /**
     * Returns the number of all bikes in the system (despite rented /taken or not)
     * @return int
     */
    public int getNumberOfAllBikes() {
        return allBikes.size();
    }

    /**
     * Returns the number of bikes not available for renting (rented / taken).
     * @return int
     */
    public int getNumberOfTakenBikes() {
        return allBikes.values().stream().mapToInt(a -> a.isTaken() ? 1 : 0).sum();
    }

    public BikeStationStateDTO getStationsStateDTO(String id) throws BikeStationNotFoundException {
        BikeStation bikeStation = allStations.get(id);
        if (bikeStation != null) return bikeStation.getBikeStationStateDTO();
        else throw new BikeStationNotFoundException("Station not found.");
    }

    public List<BikeStationStateDTO> getAllStationsStateDTO() {
        return allStations.values().stream()
                .map(BikeStation::getBikeStationStateDTO)
                .collect(Collectors.toList());
    }

    public List<BikeStandDTO> getAllStationStandsDTO(String id) throws BikeStationNotFoundException {
        BikeStation station = allStations.get(id);
        if (station != null) {
            return station.getStands().values().stream()
                    .map(BikeStand::getBikeStandDTO)
                    .collect(Collectors.toList());
        } else throw new BikeStationNotFoundException("Station not found.");
    }

    public BikeStationStateDTO addBikeStation(String name, int noStands) throws BikeStationAddException {
        if ((name == null) || (name.trim().length()<1))
            throw new BikeStationAddException("Bike name cannot be null or empty.");
        if (noStands < 1)
            throw new BikeStationAddException("Number of stands can't be negative value.");
        else {
            BikeStation newStation = new BikeStation(name, noStands, false);
            allStations.put(newStation.getId(), newStation);
            return newStation.getBikeStationStateDTO();
        }
    }

    public BikeStationStateDTO updateBikeStation(String id, BikeStationUpdateDTO dto) throws BikeStationNotFoundException {
        BikeStation toUpdate = allStations.get(id);
        if (toUpdate == null)
            throw new BikeStationNotFoundException("Station not found.");
        else return toUpdate.update(dto);
    }

    public BikeStationStateDTO deleteBikeStation(String id) throws BikeStationNotFoundException {
        BikeStation removed = allStations.remove(id);
        if (removed != null) {
            //mark the bikes taken (without removing from the system);
            for (Bike bike : removed.getAvailableBikes().values()) {
                bike.take();
            }
            return removed.getBikeStationStateDTO();
        } else throw new BikeStationNotFoundException("Station not found.");
    }

    public synchronized BikeStationStateDTO rentTheBike(String stationId, BikeDTO bike) throws BikeNotFoundException, BikeStandNotFoundException, BikeStationNotFoundException, BikeStationClientException, BikeNotTakenException, BikeNotAvailableException, BikeInconsistentStateException {
        logger.debug(Instant.now() + ", TID=" +Thread.currentThread().getId() + " - renting: " + bike);
        Bike bikeToRent = allBikes.get(bike.getBikeId());
        if (bikeToRent == null) throw new BikeNotFoundException("Bike not found.");
        if (bikeToRent.isTaken()) throw new BikeInconsistentStateException("Already rented.");
        BikeStation rentStation = allStations.get(stationId);
        if (rentStation != null) {
            if (bike.getStandId() != null) {
                BikeStand stand = rentStation.getStands().get(bike.getStandId());
                if (stand == null) throw new BikeStandNotFoundException("Stand not found.");
                rentStation.rentTheBike(bikeToRent);
            }
        } else throw new BikeStationNotFoundException("Station not found.");
        logger.debug(Instant.now() + ", TID=" +Thread.currentThread().getId() + " - rented: " + bike);
        return rentStation.getBikeStationStateDTO();
    }

    public synchronized BikeStationStateDTO returnTheBike(String stationId, BikeDTO bike) throws BikeNotFoundException, BikeStandNotFoundException, BikeStationNotFoundException, BikeNotReturnedException, BikeStationClientException, BikeStandOccupiedException, BikeInconsistentStateException {
        logger.debug(Instant.now() + ", TID=" +Thread.currentThread().getId() + " - returning: " + bike);
        Bike bikeToReturn = allBikes.get(bike.getBikeId());
        if (bikeToReturn == null) throw new BikeNotFoundException("Bike not found.");
        if (!bikeToReturn.isTaken()) throw new BikeInconsistentStateException("Already returned.");
        BikeStation returnStation = allStations.get(stationId);
        if (returnStation != null) {
            if (bike.getStandId() != null) {
                BikeStand stand = returnStation.getStands().get(bike.getStandId());
                if (stand == null) throw new BikeStandNotFoundException("Stand not found.");
                //return bike to the stand
                returnStation.returnTheBike(bikeToReturn, stand);
            } else {
                //return bike to the station
                returnStation.returnTheBike(bikeToReturn);
            }
        } else throw new BikeStationNotFoundException("Station with the given id does not exist.");
        logger.debug(Instant.now() + ", TID=" +Thread.currentThread().getId() + " - returned: " + bike);
        return returnStation.getBikeStationStateDTO();
    }

}
