package pl.wiewiora.michal.citybikes.model;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.FORBIDDEN, reason = "Inconsistent action.")
public class BikeInconsistentStateException extends BikeStationException {

    public BikeInconsistentStateException(String errorMessage) {
        super(errorMessage);
    }
}
