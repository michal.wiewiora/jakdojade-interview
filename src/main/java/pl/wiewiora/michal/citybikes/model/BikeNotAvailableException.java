package pl.wiewiora.michal.citybikes.model;

public class BikeNotAvailableException extends BikeStationException {

    public BikeNotAvailableException(String errorMessage) {
        super(errorMessage);
    }
}
