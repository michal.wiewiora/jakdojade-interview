package pl.wiewiora.michal.citybikes.model;

public class BikeStationException extends Exception {

    public BikeStationException(String errorMessage) {
        super(errorMessage);
    }

    public BikeStationException(String errorMessage, Throwable t) {
        super(errorMessage, t);
    }
}
