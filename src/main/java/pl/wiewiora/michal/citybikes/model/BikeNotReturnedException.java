package pl.wiewiora.michal.citybikes.model;

public class BikeNotReturnedException extends BikeStationException {

    public BikeNotReturnedException(String errorMessage) {
        super(errorMessage);
    }
}
