package pl.wiewiora.michal.citybikes.model;

public class BikeNotTakenException extends BikeStationException {

    public BikeNotTakenException(String errorMessage) {
        super(errorMessage);
    }
}
