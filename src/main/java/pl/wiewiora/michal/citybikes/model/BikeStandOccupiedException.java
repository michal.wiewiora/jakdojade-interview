package pl.wiewiora.michal.citybikes.model;

public class BikeStandOccupiedException extends BikeStationException {

    public BikeStandOccupiedException(String errorMessage) {
        super(errorMessage);
    }
}
