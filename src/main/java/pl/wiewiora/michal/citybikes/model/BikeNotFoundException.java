package pl.wiewiora.michal.citybikes.model;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Bike not found.")
public class BikeNotFoundException extends BikeStationException {

    public BikeNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
