package pl.wiewiora.michal.citybikes.model;

import pl.wiewiora.michal.citybikes.dto.BikeDTO;

import java.time.Instant;
import java.util.UUID;

public class Bike {

    private final String id = UUID.randomUUID().toString();
    private Boolean taken = false;
    private BikeStand parkStand;
    private BikeStation parkStation;

    public Boolean isTaken() {
        return taken;
    }

    public String getId() {
        return id;
    }

    synchronized void take() {
        this.taken = true;
        this.parkStand = null;
        this.parkStation = null;
    }

    synchronized void park(BikeStand toStand) {
        this.taken = false;
        this.parkStand = toStand;
        this.parkStation = toStand.getParentStation();

    }

    synchronized void park(BikeStation toStation) {
        long currentThreadId = Thread.currentThread().getId();
        if (!this.taken) System.err.println(Instant.now() + ", TID=" +currentThreadId + ", " +"CONFLICT2: " + id + ", " + this.taken);
        this.taken = false;
        this.parkStand = null;
        this.parkStation = toStation;
    }

    /**
     * ParkStand of the parked bicycle.
     *
     * @return null once taken or parked by the full station (no stands are available).
     * To retrieve the station call getParkStation().
     */
    public BikeStand getParkStand() {
        return parkStand;
    }

    public BikeStation getParkStation() {
        return parkStation;
    }

    public BikeDTO toBikeDTO() {
        BikeDTO dto = new BikeDTO();
        dto.setBikeId(id);
        dto.setTaken(taken);
        dto.setStandId((parkStand!=null)?parkStand.getId():null);
        dto.setStationId((parkStation!=null)?parkStation.getId():null);
        return dto;
    }

}
