package pl.wiewiora.michal.citybikes.model;

public class BikeStationAddException extends BikeStationException {

    public BikeStationAddException(String errorMessage) {
        super(errorMessage);
    }
}
