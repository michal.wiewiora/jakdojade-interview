package pl.wiewiora.michal.citybikes.model;

import pl.wiewiora.michal.citybikes.dto.BikeStandDTO;

import java.util.UUID;

public class BikeStand {

    private final String id = UUID.randomUUID().toString();
    private final BikeStation parentStation;
    private Bike parkedBike;

    public BikeStand(BikeStation parentStation) {
        this.parentStation = parentStation;
    }

    public BikeStand(BikeStation parentStation, Bike parkedBike) {
        this.parentStation = parentStation;
        this.parkedBike = parkedBike;
    }

    public BikeStandDTO getBikeStandDTO() {
        BikeStandDTO dto = new BikeStandDTO();
        dto.setStandId(this.id);
        dto.setParentStationId((parentStation!=null)?parentStation.getId():null);
        dto.setParkedBikeId((parkedBike!=null)?parkedBike.getId():null);
        return dto;
    }

    public String getId() {
        return id;
    }

    public Boolean isOccupied() {
        return parkedBike!=null;
    }

    public Boolean isFree() {
        return !isOccupied();
    }

    synchronized void bikeTaken(Bike bike) throws BikeNotAvailableException {
        if (bike != null && parkedBike != null && bike.getId().equals(parkedBike.getId())) this.parkedBike = null;
        else throw new BikeNotAvailableException("Wrong bike.");
    }

    synchronized void bikeReturned(Bike bike) throws BikeStandOccupiedException {
        if (this.parkedBike != null) throw new BikeStandOccupiedException("Bike stand is occupied.");
        this.parkedBike = bike;
    }

    public Bike getParkedBike() {
        return this.parkedBike;
    }

    public BikeStation getParentStation() {
        return parentStation;
    }
}
