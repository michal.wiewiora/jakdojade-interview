package pl.wiewiora.michal.citybikes.model;

import pl.wiewiora.michal.citybikes.dto.BikeStationStateDTO;
import pl.wiewiora.michal.citybikes.dto.BikeStationUpdateDTO;
import pl.wiewiora.michal.citybikes.integration.BikeStationClient;
import pl.wiewiora.michal.citybikes.integration.BikeStationClientException;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class BikeStation {

    public static final byte DEFAULT_NUMBER_OF_STANDS = 10;

    private final String id = UUID.randomUUID().toString();
    private String name;

    private final Map<String, BikeStand> stands = Collections.synchronizedMap(new HashMap<>());
    //bikes parked directly to the station (outside the station stands)
    private final Map<String, Bike> generalStationStand = Collections.synchronizedMap(new HashMap<>());

    public Map<String, Bike> getGeneralStationStand() {
        return generalStationStand;
    }

    //external API Gateway
    private BikeStationClient bikeStationClientGateway;

    void setBikeStationClientGateway(BikeStationClient bikeStationClientGateway) {
        this.bikeStationClientGateway = bikeStationClientGateway;
    }

    /**
     * Creates the BikeStation with the default number of stands (BikeStation.DEFAULT_NUMBER_OF_STANDS).
     * @param name - BikeStation name
     * @param withBike - true will crate BikeStation with bikes (each for the created stand)
     */
    public BikeStation(String name, boolean withBike) {
        this(name, DEFAULT_NUMBER_OF_STANDS, withBike);
    }

    /**
     * Creates the BikeStation with the given number of stands (BikeStation.DEFAULT_NUMBER_OF_STANDS).
     * @param name - BikeStation name
     * @param numberOfStands - number of stands that the station will contain
     * @param withBike - true will crate BikeStation with bikes (each for the created stand)
     */
    public BikeStation(String name, int numberOfStands, boolean withBike) {
        addStands(numberOfStands, withBike);
        this.name = name;
    }

    public String getId() {
        return this.id;
    }

    public Map<String, BikeStand> getStands() {
        return stands;
    }

    public Map<String, Bike> getAvailableBikes() {
        Map<String, Bike> availableBikes = stands.values().stream()
                .map(BikeStand::getParkedBike)
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(Bike::getId, Function.identity()));
                availableBikes.putAll(generalStationStand);
        return availableBikes;
    }

    private void addStands(int numberOfStands, boolean withBike) {
        BikeStand stand;
        Bike bike;
        for(int i=0;i<numberOfStands;i++) {
            if (withBike) {
                bike = new Bike();
                stand = new BikeStand(this, bike);
                bike.park(stand);
            } else {
                stand = new BikeStand(this);
            }
            stands.put(stand.getId(), stand);
        }
    }

    public BikeStationStateDTO getBikeStationStateDTO() {
        BikeStationStateDTO state = new BikeStationStateDTO(this.name);
        state.setId(this.id);
        state.setAllStands(this.stands.size());
        state.setFreeStands(calculateFreeStands());
        state.setOccupiedStands(calculateOccupiedStands());
        state.setAvailableBikes(getAvailableBikes().size());
        return state;
    }

    private int calculateFreeStands() {
        return (int) stands.values().stream()
                .filter(BikeStand::isFree)
                .count();
    }

    private int calculateOccupiedStands() {
        return (int) stands.values().stream()
                .filter(BikeStand::isOccupied)
                .count();
    }

    /**
     * Updates the BikeStation: <br/>
     *  <li/> stands - number of stands. Bikes from removed stands (if decreasing) goes to the station.
     *  <li/> name - station name.
     *
     * @param station - station data (DTO)
     */
    public synchronized BikeStationStateDTO update(BikeStationUpdateDTO station) {
        //update number of stands
        int noStandsChange = station.getAllStands() - stands.size();
        if (noStandsChange < 0) this.removeStands(Math.abs(noStandsChange));
        else if ((noStandsChange > 0)) this.addStands(noStandsChange, false);
        //update name
        this.name = station.getStationName();
        return this.getBikeStationStateDTO();
    }

    private void removeStands(int removeCount) {
        if (removeCount == 0) return;
        List<String> idsToRemove = stands.keySet().stream()
                .limit(removeCount)
                .collect(Collectors.toList());

        for (String id : idsToRemove) {
            BikeStand removed = stands.remove(id);
            Bike parkedBike = removed.getParkedBike();
            if (parkedBike != null) parkedBike.park(this);
        }
    }

    /**
     * External API call, to check if the bike was physically taken from the stand
     */
    private boolean isBikeTakenFromTheStand(BikeStand stand) throws BikeStationClientException {
        return this.bikeStationClientGateway.bikeTakenFromTheStand(stand.getId(), stand.getParkedBike().getId());
    }

    /**
     * External API call, to check if the bike was physically returned to the stand
     */
    private boolean isBikeReturnedToTheStand(BikeStand stand, Bike bike) throws BikeStationClientException {
            return this.bikeStationClientGateway.bikeReturnedToTheStand(stand.getId(), bike.getId());
    }

    /**
     * External API call, to check if the bike was physically taken from the station
     */
    private boolean isBikeTakenFromTheStation(Bike bike) throws BikeStationClientException {
        return this.bikeStationClientGateway.bikeTakenFromTheStation(this.id, bike.getId());
    }

    /**
     * External API call, to check if the bike was physically returned to the station
     */
    private boolean isBikeReturnedToTheStation(Bike bike) throws BikeStationClientException {
        return this.bikeStationClientGateway.bikeReturnedToTheStation(this.id, bike.getId());
    }

    private synchronized boolean stationNotContainsTheBike(Bike bike) {
        return !this.generalStationStand.containsKey(bike.getId());
    }

    /**
     * Rents the bike.
     * @param bike - bike to rent
     * @throws BikeNotAvailableException, BikeStationClientException, BikeNotTakenException
     */
    public synchronized void rentTheBike(Bike bike) throws BikeNotAvailableException, BikeStationClientException, BikeNotTakenException {
        BikeStation station = bike.getParkStation();
        BikeStand stand = bike.getParkStand();
        if (bike.isTaken()) throw new BikeNotAvailableException("Bike is unavailable.");
        if (stand != null) {
            //rent the bike from the stand
            if (stand.isFree()) throw new BikeNotAvailableException("Stand is empty.");
            if (isBikeTakenFromTheStand(stand)) {
                bike.take();
                stand.bikeTaken(bike);
            }
            else throw new BikeNotTakenException("Bike not taken from the stand.");
        } else if (station != null) {
            //rent the bike near the station
            if (stationNotContainsTheBike(bike)) throw new BikeNotAvailableException("Bike not found.");
            if (isBikeTakenFromTheStation(bike)) {
                bike.take();
                this.generalStationStand.remove(bike.getId());
            }
            else throw new BikeNotTakenException("Bike not taken from the station.");
        } else throw new BikeNotAvailableException("Bike doesn't belong to the registered station.");

    }

    /**
     * Returns rented bike to the stand
     * @param bike - bike to return
     * @param stand - stand of returning
     * @throws BikeStandOccupiedException, BikeStationClientException, BikeNotReturnedException
     */
    public synchronized void returnTheBike(Bike bike, BikeStand stand) throws BikeStandOccupiedException, BikeStationClientException, BikeNotReturnedException {
        if (stand.isOccupied()) throw new BikeStandOccupiedException("Stand is occupied.");
        if (isBikeReturnedToTheStand(stand, bike)) {
            bike.park(stand);
            stand.bikeReturned(bike);
        }
        else throw new BikeNotReturnedException("Bike not returned to the stand.");
    }

    /**
     * Returns rented bike to the station (no available stands)
     * @param bike - bike to return
     * @throws BikeNotReturnedException, BikeStationClientException
     */
    public synchronized void returnTheBike(Bike bike) throws BikeNotReturnedException, BikeStationClientException {
        if (isBikeReturnedToTheStation(bike)) {
            bike.park(this);
            this.generalStationStand.put(bike.getId(), bike);
        }
        else throw new BikeNotReturnedException("Bike was not returned to the station.");
    }

}
