package pl.wiewiora.michal.citybikes.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pl.wiewiora.michal.citybikes.dto.*;
import pl.wiewiora.michal.citybikes.integration.BikeStationClientException;
import pl.wiewiora.michal.citybikes.model.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class BikeStationRestController {

    @Autowired
    private BikeStationService bikeStationService;

    @GetMapping("/bike-stations")
    public List<BikeStationStateDTO> all() {
        return bikeStationService.getAllStationsStateDTO();
    }

    @GetMapping("/bike-stations/{id}")
    public BikeStationStateDTO get(@PathVariable String id) throws BikeStationNotFoundException {
        return bikeStationService.getStationsStateDTO(id);
    }

    @DeleteMapping("/bike-stations/{id}")
    public BikeStationStateDTO delete(@PathVariable String id) throws BikeStationNotFoundException {
        return bikeStationService.deleteBikeStation(id);
    }

    @PostMapping("/bike-stations")
    public BikeStationStateDTO add(@RequestBody @Valid BikeStationCreateDTO station) {
        try {
            return bikeStationService.addBikeStation(station.getStationName(), station.getAllStands());
        } catch (BikeStationAddException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PutMapping("/bike-stations/{id}")
    public BikeStationStateDTO save(@PathVariable String id, @RequestBody @Valid BikeStationUpdateDTO dto) throws BikeStationNotFoundException {
        return bikeStationService.updateBikeStation(id, dto);
    }

    @GetMapping("/bike-stations/{id}/stands")
    public List<BikeStandDTO> getStands(@PathVariable String id) throws BikeStationNotFoundException {
        return bikeStationService.getAllStationStandsDTO(id);
    }

    @PutMapping("/bike-stations/{id}/rent")
    public BikeStationStateDTO rentBike(@PathVariable String id, @RequestBody @Valid BikeDTO bike) throws BikeNotFoundException, BikeStandNotFoundException, BikeStationNotFoundException, BikeStationClientException, BikeInconsistentStateException {
        try {
            return bikeStationService.rentTheBike(id, bike);
        } catch (BikeNotTakenException | BikeNotAvailableException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PutMapping("/bike-stations/{id}/return")
    public BikeStationStateDTO returnBike(@PathVariable String id, @RequestBody @Valid BikeDTO bike) throws BikeNotFoundException, BikeStandNotFoundException, BikeStationNotFoundException, BikeStationClientException, BikeInconsistentStateException {
        try {
            return bikeStationService.returnTheBike(id, bike);
        } catch (BikeNotReturnedException | BikeStandOccupiedException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

}
