package pl.wiewiora.michal.citybikes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CityBikesApp {

	public static void main(String[] args) {
		SpringApplication.run(CityBikesApp.class, args);
	}

}
