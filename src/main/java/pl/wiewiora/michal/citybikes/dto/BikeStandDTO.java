package pl.wiewiora.michal.citybikes.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class BikeStandDTO implements Serializable {

    private String standId;
    private String parentStationId;
    private String parkedBikeId;

}
