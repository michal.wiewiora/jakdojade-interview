package pl.wiewiora.michal.citybikes.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class BikeStationStateDTO implements Serializable {

    private String id;
    private String stationName;
    private int allStands = 0;
    private int freeStands = 0;
    private int occupiedStands = 0;
    //additional (more then required) field to monitor number of the bikes that are not parked near the stands
    private int availableBikes = 0;

    public BikeStationStateDTO(String stationName) {
        this.stationName = stationName;
    }
}
