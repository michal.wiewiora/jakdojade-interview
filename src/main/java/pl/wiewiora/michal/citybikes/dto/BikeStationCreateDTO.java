package pl.wiewiora.michal.citybikes.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
public class BikeStationCreateDTO implements Serializable {

    @NotNull
    @NotEmpty
    private String stationName;
    @NotNull
    @Min(1)
    private int allStands;
}
