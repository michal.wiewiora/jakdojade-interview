package pl.wiewiora.michal.citybikes.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.io.Serializable;

@Getter
@Setter
@ToString
public class BikeDTO implements Serializable {

    @NotNull
    @NotEmpty
    private String bikeId;
    @Null
    private Boolean taken;
    private String stationId;
    private String standId;

}
