package pl.wiewiora.michal.citybikes.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class BikeStationUpdateDTO implements Serializable {

    private String stationName;
    private int allStands;
}
