package pl.wiewiora.michal.citybikes.integration;

import org.springframework.stereotype.Service;

/**
 * External service calls.
 *
 */
@Service
public class BikeStationClientImpl implements BikeStationClient {

    @Override
    public Boolean bikeTakenFromTheStand(String standId, String bikeId) {
        //TODO: to implement the integration part
        return true;
    }

    @Override
    public Boolean bikeReturnedToTheStand(String standId, String bikeId) {
        //TODO: to implement the integration part
        return true;
    }

    @Override
    public Boolean bikeTakenFromTheStation(String stationId, String bikeId) {
        //TODO: to implement the integration part
        return true;
    }

    @Override
    public Boolean bikeReturnedToTheStation(String stationId, String bikeId) {
        //TODO: to implement the integration part
        return true;
    }
}
