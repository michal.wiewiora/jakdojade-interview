package pl.wiewiora.michal.citybikes.integration;

public interface BikeStationClient {

    Boolean bikeTakenFromTheStand(String standId, String bikeId) throws BikeStationClientException;

    Boolean bikeReturnedToTheStand(String standId, String bikeId) throws BikeStationClientException;

    Boolean bikeTakenFromTheStation(String stationId, String bikeId) throws BikeStationClientException;

    Boolean bikeReturnedToTheStation(String stationId, String bikeId) throws BikeStationClientException;
}
