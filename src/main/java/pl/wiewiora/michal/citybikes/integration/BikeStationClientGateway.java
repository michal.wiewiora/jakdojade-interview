package pl.wiewiora.michal.citybikes.integration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.*;

@Service
public class BikeStationClientGateway implements BikeStationClient {

    public static final int GATEWAY_API_THREADS_COUNT = 10;
    private final ExecutorService executor = Executors.newFixedThreadPool(GATEWAY_API_THREADS_COUNT);

    @Autowired
    public BikeStationClientImpl bikeStationClient;

    /**
     * External API call that opens the stand electric-lock and confirms if the bike was taken
     *
     * @param standId - stand Id
     * @return true|false
     */
    public Boolean bikeTakenFromTheStand(String standId, String bikeId) throws BikeStationClientException {
        Boolean bikeWasTaken;
        try {
            Future<Boolean> future = executor.submit(() -> bikeStationClient.bikeTakenFromTheStand(standId, bikeId));
            bikeWasTaken = future.get(2, TimeUnit.SECONDS);
        } catch (InterruptedException| ExecutionException | TimeoutException e) {
            e.printStackTrace();
            throw new BikeStationClientException("Error: " + e.getMessage(), e);
        }
        return bikeWasTaken;
    }

    /**
     * External API call that checks if the bike was returned and electric-lock was closed
     *
     * @param standId - stand Id
     * @return true|false
     */
    public Boolean bikeReturnedToTheStand(String standId, String bikeId) throws BikeStationClientException {
        Boolean bikeWasReturned;
        try {
            Future<Boolean> future = executor.submit(() -> bikeStationClient.bikeReturnedToTheStand(standId, bikeId));
            bikeWasReturned = future.get(2, TimeUnit.SECONDS);
        } catch (InterruptedException| ExecutionException | TimeoutException e) {
            e.printStackTrace();
            throw new BikeStationClientException("Error: " + e.getMessage(), e);
        }
        return bikeWasReturned;
    }

    /**
     * External API call that checks if the bike lock was released and the bike was taken from the station
     *
     * @param stationId - station Id
     * @return true|false
     */
    public Boolean bikeTakenFromTheStation(String stationId, String bikeId) throws BikeStationClientException {
        Boolean bikeWasTaken;
        try {
            Future<Boolean> future = executor.submit(() -> bikeStationClient.bikeTakenFromTheStation(stationId, bikeId));
            bikeWasTaken = future.get(2, TimeUnit.SECONDS);
        } catch (InterruptedException| ExecutionException | TimeoutException e) {
            e.printStackTrace();
            throw new BikeStationClientException("Error: " + e.getMessage(), e);
        }
        return bikeWasTaken;
    }

    /**
     * External API call that will check if the bike lock was secured and the bike was returned to the station
     *
     * @param stationId - station Id
     * @return true|false
     */
    public Boolean bikeReturnedToTheStation(String stationId, String bikeId) throws BikeStationClientException {
        Boolean bikeWasReturned;
        try {
            Future<Boolean> future = executor.submit(() -> bikeStationClient.bikeReturnedToTheStation(stationId, bikeId));
            bikeWasReturned = future.get(2, TimeUnit.SECONDS);
        } catch (InterruptedException| ExecutionException | TimeoutException e) {
            e.printStackTrace();
            throw new BikeStationClientException("Error: " + e.getMessage(), e);
        }
        return bikeWasReturned;
    }
}