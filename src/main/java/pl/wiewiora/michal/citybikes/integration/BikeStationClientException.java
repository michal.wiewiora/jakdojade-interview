package pl.wiewiora.michal.citybikes.integration;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.wiewiora.michal.citybikes.model.BikeStationException;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Internal system error.")
public class BikeStationClientException extends BikeStationException {

    public BikeStationClientException(String errorMessage) {
        super(errorMessage);
    }

    public BikeStationClientException(String errorMessage, Throwable t) {
        super(errorMessage, t);
    }
}
