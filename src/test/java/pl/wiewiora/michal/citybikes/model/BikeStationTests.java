package pl.wiewiora.michal.citybikes.model;

import org.junit.jupiter.api.Test;
import pl.wiewiora.michal.citybikes.dto.BikeStationStateDTO;
import pl.wiewiora.michal.citybikes.dto.BikeStationUpdateDTO;
import pl.wiewiora.michal.citybikes.integration.BikeStationClient;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


public class BikeStationTests {

    @Test
    public void createDefaultBikeStationWithoutBikes() {
        //given
        BikeStation station = new BikeStation("test_name", false);
        //when
        //then
        assertEquals(station.getStands().size(), BikeStation.DEFAULT_NUMBER_OF_STANDS);
        assertNotNull(station.getId());
        assertEquals(station.getAvailableBikes().size(), 0);
    }

    @Test
    public void createDefaultBikeStationWithBikes() {
        //given
        BikeStation station = new BikeStation("test_name", true);
        //when
        //then
        assertEquals(station.getStands().size(), BikeStation.DEFAULT_NUMBER_OF_STANDS);
        assertNotNull(station.getId());
        assertEquals(station.getAvailableBikes().size(), BikeStation.DEFAULT_NUMBER_OF_STANDS);
    }

    @Test
    public void createBikeStationWithDefinedNumberOfStandsWithBikes() {
        //given
        int numberOfStands = 13;
        BikeStation station = new BikeStation("test_name", numberOfStands, true);
        //when
        //then
        assertEquals(station.getStands().size(), numberOfStands);
        assertNotNull(station.getId());
        assertEquals(station.getAvailableBikes().size(), numberOfStands);
    }

    @Test
    public void createDefaultBikeStationAndCheckBikeStationStateDTOValue() {
        //given
        BikeStation station = new BikeStation("test_name", false);
        //when
        BikeStationStateDTO dto = station.getBikeStationStateDTO();
        //then
        assertEquals(dto.getId(), station.getId());
        assertEquals(dto.getStationName(), "test_name");
        assertEquals(dto.getAllStands(), BikeStation.DEFAULT_NUMBER_OF_STANDS);
        assertEquals(dto.getFreeStands(), BikeStation.DEFAULT_NUMBER_OF_STANDS);
        assertEquals(dto.getOccupiedStands(), 0);
    }

    @Test
    public void updateTheStationByChangingNameAndDecreasingNumberOfStands() {
        //given
        BikeStation station = new BikeStation("test_name", false);
        BikeStationUpdateDTO updateDto = new BikeStationUpdateDTO();
        updateDto.setStationName("updated_name");
        updateDto.setAllStands(3);
        //when
        station.update(updateDto);
        //then
        BikeStationStateDTO dto = station.getBikeStationStateDTO();
        assertEquals(dto.getStationName(), "updated_name");
        assertEquals(dto.getAllStands(), 3);
    }

    @Test
    public void updateTheStationByIncreasingTheNumberOfStands() {
        //given
        BikeStation station = new BikeStation("test_name", false);
        BikeStationUpdateDTO updateDto = new BikeStationUpdateDTO();
        updateDto.setAllStands(13);
        //when
        station.update(updateDto);
        //then
        BikeStationStateDTO dto = station.getBikeStationStateDTO();
        assertEquals(dto.getAllStands(), 13);
    }

    @Test
    public void rentTheBikeFromTheStation() {
        //given
        BikeStation station = new BikeStation("test_name", true);
        Bike bikeToRent = station.getAvailableBikes().values().iterator().next();
        BikeStationClient mock = mock(BikeStationClient.class);
        //when
        //then
        assertDoesNotThrow(() -> {
            when(mock.bikeTakenFromTheStand(any(), any())).thenReturn(true);
            //when(mock.bikeReturnedToTheStand(any(), any())).thenReturn(false);
            //when(mock.bikeTakenFromTheStation(any(), any())).thenReturn(true);
            //when(mock.bikeReturnedToTheStation(any(), any())).thenReturn(false);
        });
        station.setBikeStationClientGateway(mock);
        assertDoesNotThrow(() -> station.rentTheBike(bikeToRent));
        assertTrue(bikeToRent.isTaken());
    }

    @Test
    public void rentTheBikeFromTheEmptyStation() {
        //given
        Bike bike = new Bike();
        BikeStation station = new BikeStation("test_name", false);
        //then
        Exception e = assertThrows(BikeNotAvailableException.class, () -> station.rentTheBike(bike));
        //then
        assertEquals("Bike doesn't belong to the registered station.", e.getMessage());
    }

    @Test
    public void rentTheDifferentBikeThanParkedNearTheStationStand() {
        //given
        Bike bike = new Bike();
        BikeStation station = new BikeStation("test_name", true);
        //then
        Exception e = assertThrows(BikeNotAvailableException.class, () -> station.rentTheBike(bike));
        //then
        assertEquals("Bike doesn't belong to the registered station.", e.getMessage());
    }


    @Test
    public void returnTheBikeToTheStation() {
        //given
        BikeStation station = new BikeStation("test_name", true);
        Bike bike = new Bike();
        bike.take();
        BikeStationClient mock = mock(BikeStationClient.class);
        //when
        //then
        assertDoesNotThrow(() -> {
            //when(mock.bikeTakenFromTheStand(any(), any())).thenReturn(true);
            //when(mock.bikeReturnedToTheStand(any(), any())).thenReturn(false);
            //when(mock.bikeTakenFromTheStation(any(), any())).thenReturn(true);
            when(mock.bikeReturnedToTheStation(any(), any())).thenReturn(true);
        });
        station.setBikeStationClientGateway(mock);
        assertDoesNotThrow(() -> station.returnTheBike(bike));
        assertFalse(bike.isTaken());
    }

    @Test
    public void returnTheBikeToTheEmptyStationStand() {
        //given
        BikeStation station = new BikeStation("test_name", false);
        BikeStand stand = station.getStands().values().iterator().next();
        Bike bike = new Bike();
        bike.take();
        BikeStationClient mock = mock(BikeStationClient.class);
        //when
        //then
        assertDoesNotThrow(() -> {
            //when(mock.bikeTakenFromTheStand(any(), any())).thenReturn(true);
            when(mock.bikeReturnedToTheStand(any(), any())).thenReturn(true);
            //when(mock.bikeTakenFromTheStation(any(), any())).thenReturn(true);
            //when(mock.bikeReturnedToTheStation(any(), any())).thenReturn(true);
        });
        station.setBikeStationClientGateway(mock);
        assertDoesNotThrow(() -> station.returnTheBike(bike, stand));
        assertFalse(bike.isTaken());
    }

    @Test
    public void returnTheBikeToTheOccupiedStationStand() {
        //given
        BikeStation station = new BikeStation("test_name", true);
        BikeStand stand = station.getStands().values().iterator().next();
        Bike bike = new Bike();
        //when
        Exception e = assertThrows(BikeStandOccupiedException.class, () ->  station.returnTheBike(bike, stand));
        //then
        assertEquals("Stand is occupied.", e.getMessage());
    }

}
