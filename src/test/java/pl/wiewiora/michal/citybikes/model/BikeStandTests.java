package pl.wiewiora.michal.citybikes.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.wiewiora.michal.citybikes.dto.BikeStandDTO;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class BikeStandTests {

    private BikeStation bikeStation;
    private final Random random = new Random();

    @BeforeEach
    public void setUp(){
        //given
        bikeStation = new BikeStation("test", random.nextBoolean());
    }

    @Test
    public void createEmptyBikeStandAndCheckPropertiesInitialization() {
        //given
        BikeStand emptyStand = new BikeStand(bikeStation);
        //when
        //then
        assertNotNull(emptyStand.getId());
        assertNull(emptyStand.getParkedBike());
        assertEquals(emptyStand.getParentStation(), bikeStation);
        assertTrue(emptyStand.isFree());
        assertFalse(emptyStand.isOccupied());
    }

    @Test
    public void createEmptyBikeStandGenerateDTOAndCheckDTOProperties() {
        //given
        BikeStand emptyStand = new BikeStand(bikeStation);
        //when
        BikeStandDTO dto = emptyStand.getBikeStandDTO();
        //then
        assertEquals(emptyStand.getParentStation().getId(), dto.getParentStationId());
        assertEquals(emptyStand.getId(), dto.getStandId());
        assertNull(dto.getParkedBikeId());
    }

    @Test
    public void rentTheBikeFromTheEmptyStand() {
        //given
        Bike bike = new Bike();
        BikeStand emptyStand = new BikeStand(bikeStation);
        //when
        Exception e = assertThrows(BikeNotAvailableException.class, () -> emptyStand.bikeTaken(bike));
        //then
        assertEquals("Wrong bike.", e.getMessage());
    }

    @Test
    public void returnTheBikeToTheEmptyStand() {
        //given
        Bike bike = new Bike();
        BikeStand emptyStand = new BikeStand(bikeStation);
        //when
        assertDoesNotThrow(() -> emptyStand.bikeReturned(bike));
        //then
        assertEquals(bike, emptyStand.getParkedBike());
        assertTrue(emptyStand.isOccupied());
    }

    @Test
    public void createBikeStandWithBikeAndCheckPropertiesInitialization() {
        //given
        Bike bike = new Bike();
        BikeStand occupiedStand = new BikeStand(bikeStation, bike);
        //when
        //then
        assertNotNull(occupiedStand.getId());
        assertEquals(occupiedStand.getParkedBike(), bike);
        assertEquals(occupiedStand.getParentStation(), bikeStation);
        assertFalse(occupiedStand.isFree());
        assertTrue(occupiedStand.isOccupied());
    }

    @Test
    public void createBikeWithBikeStandGenerateDTOAndCheckDTOProperties() {
        //given
        Bike bike = new Bike();
        BikeStand occupiedStand = new BikeStand(bikeStation, bike);
        //when
        BikeStandDTO dto = occupiedStand.getBikeStandDTO();
        //then
        assertEquals(occupiedStand.getParentStation().getId(), dto.getParentStationId());
        assertEquals(occupiedStand.getId(), dto.getStandId());
        assertEquals(occupiedStand.getParkedBike().getId(), dto.getParkedBikeId());
    }

    @Test
    public void rentTheBikeFromOccupiedStand() {
        //given
        Bike bike = new Bike();
        BikeStand occupiedStand = new BikeStand(bikeStation, bike);
        //when
        assertDoesNotThrow(() -> occupiedStand.bikeTaken(bike));
        //then
        assertTrue(occupiedStand.isFree());
        assertFalse(occupiedStand.isOccupied());
    }

    @Test
    public void rentTheDifferentBikeFromOccupiedStand() {
        //given
        Bike bike1 = new Bike();
        Bike bike2 = new Bike();
        BikeStand occupiedStand = new BikeStand(bikeStation, bike1);
        //when
        Exception e = assertThrows(BikeNotAvailableException.class, () -> occupiedStand.bikeTaken(bike2));
        //then
        assertEquals("Wrong bike.", e.getMessage());
    }

    @Test
    public void returnTheBikeToOccupiedStand() {
        //given
        Bike bike = new Bike();
        BikeStand occupiedStand = new BikeStand(bikeStation, bike);
        //when
        Exception e = assertThrows(BikeStandOccupiedException.class, () -> occupiedStand.bikeReturned(bike));
        //then
        assertEquals("Bike stand is occupied.", e.getMessage());
    }

}
