package pl.wiewiora.michal.citybikes.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class BikeTests {

    private Bike bike;
    private final Random random = new Random();

    @BeforeEach
    public void setUp(){
        //given
        bike = new Bike();
    }

    @Test
    public void createBikeAndCheckPropertiesInitialization() {
        //given
        //when
        //then
        assertNotNull(bike.getId());
        assertFalse(bike.isTaken());
        assertNull(bike.getParkStand());
        assertNull(bike.getParkStation());
    }

    @Test
    public void rentTheBikeAndCheckPropertiesInitialization() {
        //given
        //when
        bike.take();
        //then
        assertNotNull(bike.getId());
        assertTrue(bike.isTaken());
        assertNull(bike.getParkStand());
        assertNull(bike.getParkStation());
    }

    @Test
    public void parkTheBikeToStandAndCheckPropertiesInitialization() {
        //given
        BikeStation station = new BikeStation("testStation", random.nextBoolean());
        BikeStand stand  = new BikeStand(station);
        //when
        bike.park(stand);
        //then
        assertFalse(bike.isTaken());
        assertNotNull(bike.getParkStand());
        assertNotNull(bike.getParkStation());
    }

    @Test
    public void parkTheBikeToStationAndCheckPropertiesInitialization() {
        //given
        BikeStation station = new BikeStation("testStation", random.nextBoolean());
        //when
        bike.park(station);
        //then
        assertFalse(bike.isTaken());
        assertNull(bike.getParkStand());
        assertNotNull(bike.getParkStation());
    }

    @Test
    public void parkPreviouslyRentedBikeAndCheckPropertiesInitialization() {
        //given
        BikeStation station = new BikeStation("testStation", random.nextBoolean());
        BikeStand stand  = new BikeStand(station);
        //when
        bike.take();
        bike.park(stand);
        //then
        assertFalse(bike.isTaken());
        assertNotNull(bike.getParkStand());
        assertNotNull(bike.getParkStation());
    }

    @Test
    public void rentPreviouslyParkedBikeAndCheckPropertiesInitialization() {
        //given
        BikeStation station = new BikeStation("testStation", random.nextBoolean());
        BikeStand stand  = new BikeStand(station);
        //when
        bike.park(stand);
        bike.take();
        //then
        assertTrue(bike.isTaken());
        assertNull(bike.getParkStand());
        assertNull(bike.getParkStation());
    }

}
