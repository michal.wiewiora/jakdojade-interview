package pl.wiewiora.michal.citybikes.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pl.wiewiora.michal.citybikes.dto.BikeDTO;
import pl.wiewiora.michal.citybikes.dto.BikeStationCreateDTO;
import pl.wiewiora.michal.citybikes.dto.BikeStationUpdateDTO;
import pl.wiewiora.michal.citybikes.model.BikeStationService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = BikeStationRestController.class)
public class BikeStationRestControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private BikeStationService cityBikeService;

    /*
    Request Matching, input serialization, validation tests and statuses
    */

    @Test
    public void whenValidGetAllStationsInput_thenReturn200() throws Exception {
        mockMvc.perform(get("/bike-stations")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void whenValidGetStationInput_thenReturn200() throws Exception {
        mockMvc.perform(get("/bike-stations/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void whenValidDeleteStationInput_thenReturn200() throws Exception {
        mockMvc.perform(delete("/bike-stations/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void whenValidAddStationInput_thenReturn200() throws Exception {
        BikeStationCreateDTO dto = new BikeStationCreateDTO();
        dto.setStationName("some_name");
        dto.setAllStands(1);
        mockMvc.perform(post("/bike-stations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isOk());
    }

    @Test
    public void whenInvalidNameAddStationInput_thenReturn400() throws Exception {
        BikeStationCreateDTO dto = new BikeStationCreateDTO();
        dto.setStationName(""); //empty name
        dto.setAllStands(1);
        mockMvc.perform(post("/bike-stations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenInvalidNumberOfStandsAddStationInput_thenReturn400() throws Exception {
        BikeStationCreateDTO dto = new BikeStationCreateDTO();
        dto.setStationName("name");
        dto.setAllStands(-1); //negative number of stands
        mockMvc.perform(post("/bike-stations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenValidUpdateStationInput_thenReturn200() throws Exception {
        BikeStationUpdateDTO dto = new BikeStationUpdateDTO();
        mockMvc.perform(put("/bike-stations/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isOk());
    }

    @Test
    public void whenValidGetAllStationStandsInput_thenReturn200() throws Exception {
        mockMvc.perform(get("/bike-stations/1/stands")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void whenValidRentInput_thenReturn200() throws Exception {
        BikeDTO dto = new BikeDTO();
        dto.setBikeId("1");
        mockMvc.perform(put("/bike-stations/1/rent")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isOk());
    }

    @Test
    public void whenInvalidRentInput_thenReturn200() throws Exception {
        BikeDTO dto = new BikeDTO();
        dto.setBikeId(""); //empty
        mockMvc.perform(put("/bike-stations/1/rent")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenInvalid2RentInput_thenReturn200() throws Exception {
        BikeDTO dto = new BikeDTO();
        dto.setBikeId(null); //null
        mockMvc.perform(put("/bike-stations/1/rent")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenValidReturnInput_thenReturn200() throws Exception {
        BikeDTO dto = new BikeDTO();
        dto.setBikeId("1");
        dto.setStationId("1");
        mockMvc.perform(put("/bike-stations/1/return")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isOk());
    }

    @Test
    public void whenInvalidReturnInput_thenReturn200() throws Exception {
        BikeDTO dto = new BikeDTO();
        dto.setBikeId(null); //null id
        dto.setStationId("1");
        mockMvc.perform(put("/bike-stations/1/return")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isBadRequest());
    }

}
