package pl.wiewiora.michal.citybikes;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import pl.wiewiora.michal.citybikes.dto.*;
import pl.wiewiora.michal.citybikes.model.Bike;
import pl.wiewiora.michal.citybikes.model.BikeStand;
import pl.wiewiora.michal.citybikes.model.BikeStation;
import pl.wiewiora.michal.citybikes.model.BikeStationService;

import java.time.Instant;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

/**
 * Integration tests.
 * Checks business logic, output serialization and return HTTP statuses
 */
@SpringBootTest
@AutoConfigureMockMvc
class CityBikesTests {

	private static final Logger logger = LoggerFactory.getLogger(CityBikesTests.class);

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private BikeStationService bikeStationService;

	private final Random generator = new Random();

	private List<BikeStationStateDTO>  getAllStationsRequest() throws Exception {
		MvcResult result = mockMvc.perform(get("/bike-stations")
				.contentType(MediaType.APPLICATION_JSON)).andReturn();
		return objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<BikeStationStateDTO>>(){});
	}

	private void rentTheBikeRequest(BikeDTO bikeDto) throws Exception {
		mockMvc.perform(put("/bike-stations/" + bikeDto.getStationId() + "/rent")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(bikeDto)))
				.andReturn();
	}

	private void returnTheBikeToStationRequest(BikeDTO bikeDto, String stationId) throws Exception {
		mockMvc.perform(put("/bike-stations/" + stationId + "/return")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(bikeDto)))
				.andReturn();
	}

	public void updateBikeStationRequest(String id, BikeStationUpdateDTO dto)  throws Exception {
		mockMvc.perform(put("/bike-stations/" + id)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(dto)))
				.andReturn();
	}

	private void deleteStationRequest(String stationId) throws Exception {
		mockMvc.perform(delete("/bike-stations/" + stationId)
				.contentType(MediaType.APPLICATION_JSON))
				.andReturn();
	}

	private void addStationRequest(String name, int noStands) throws Exception {
		BikeStationCreateDTO dto = new BikeStationCreateDTO();
		dto.setStationName(name);
		dto.setAllStands(noStands);
		mockMvc.perform(post("/bike-stations")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(dto)))
				.andReturn();
	}

	private int countAllAvailableBikes(List<BikeStationStateDTO> stationList) {
		return stationList.stream().mapToInt(BikeStationStateDTO::getAvailableBikes).sum();
	}

	@Test
	public void updateTheBikeStationNameAndStands_andCheck() throws Exception {
		//list the available stations and pick two (web-request)
		List<BikeStationStateDTO> stationList = getAllStationsRequest();

		Iterator<BikeStationStateDTO> iterator = stationList.iterator();
		BikeStationStateDTO stationStateBefore = iterator.next();

		BikeStationUpdateDTO toUpdate = new BikeStationUpdateDTO();
		toUpdate.setAllStands((stationStateBefore.getAllStands() + 10)); //increase the number of stands
		toUpdate.setStationName("brand_new_name");
		updateBikeStationRequest(stationStateBefore.getId(), toUpdate);

		BikeStationStateDTO stationStateAfter = getAllStationsRequest().stream()
				.filter(dto -> dto.getId().equals(stationStateBefore.getId()))
				.collect(Collectors.toList()).get(0);

		assertEquals(stationStateAfter.getId(),stationStateBefore.getId());
		assertEquals(stationStateAfter.getAllStands(),stationStateBefore.getAllStands() + 10);
		assertEquals(stationStateAfter.getOccupiedStands(),stationStateBefore.getOccupiedStands());
		assertEquals(stationStateAfter.getStationName(), toUpdate.getStationName());
		assertEquals(stationStateAfter.getFreeStands(),stationStateAfter.getFreeStands());
		assertEquals(stationStateAfter.getAvailableBikes(),stationStateBefore.getAvailableBikes());
	}

	@Test
	public void removeStation_andCheckTheNumberOfBikes() throws Exception {
		//list the available stations
		List<BikeStationStateDTO> stationList = getAllStationsRequest();
		BikeStationStateDTO stationToRemove = stationList.iterator().next();
		int availableBikesBefore = countAllAvailableBikes(stationList);
		int takenBikesBefore = bikeStationService.getNumberOfTakenBikes();
		int bikesToBeTakenOnceDeleted = stationToRemove.getAvailableBikes();

		//delete 1 stations
		deleteStationRequest(stationToRemove.getId());

		int availableBikesAfter = countAllAvailableBikes(getAllStationsRequest());
		int takenBikesAfter = bikeStationService.getNumberOfTakenBikes();
		assertEquals(availableBikesAfter,  availableBikesBefore - bikesToBeTakenOnceDeleted);
		assertEquals(takenBikesAfter, takenBikesBefore + bikesToBeTakenOnceDeleted);
	}

	@Test
	public void addStation_andCheckTheNumberOfBikes() throws Exception {
		//list the available stations
		List<BikeStationStateDTO> stationList = getAllStationsRequest();
		int availableBikesBefore = countAllAvailableBikes(stationList);
		int allBikesBefore = bikeStationService.getNumberOfAllBikes();
		int takenBikesBefore = bikeStationService.getNumberOfTakenBikes();
		assertEquals(allBikesBefore, takenBikesBefore + availableBikesBefore);

		//add 1 station (without bikes)
		String newName = "freshly_added_station" + generator.nextInt(10);
		addStationRequest(newName, generator.nextInt(100));

		int availableBikesAfter = countAllAvailableBikes(getAllStationsRequest());
		int takenBikesAfter = bikeStationService.getNumberOfTakenBikes();
		assertEquals(takenBikesBefore, takenBikesAfter);
		assertEquals(availableBikesBefore, availableBikesAfter);
	}

	/*
	------------------------
	 CONCURRENCY TESTS
	------------------------
	*/

	private BikeDTO takeRandomBikeFromTheSystem() {
		Object[] bikes = bikeStationService.getAllBikes().values().toArray();
		return ((Bike)bikes[generator.nextInt(bikes.length)]).toBikeDTO();
	}

	private BikeStandDTO takeRandomStandFromTheSystem() {
		Object[] stations = bikeStationService.getAllStations().values().toArray();
		BikeStation station = (BikeStation)stations[generator.nextInt(stations.length)];
		Object[] stands = station.getStands().values().toArray();
		return ((BikeStand)stands[generator.nextInt(stands.length)]).getBikeStandDTO();
	}

	@Test
	public void run100RandomRentAndRandomReturnThreadsInParallel_andCheckTheSystemDataConsistencyOnceFinished() throws Exception {
		int numberOfThreads = 200;

		int availableBikesBefore = countAllAvailableBikes(getAllStationsRequest());
		int allBikesBefore = bikeStationService.getNumberOfAllBikes();
		int takenBikesBefore = bikeStationService.getNumberOfTakenBikes();
		assertEquals(allBikesBefore, takenBikesBefore + availableBikesBefore);

		ExecutorService service = Executors.newFixedThreadPool(10);
		CountDownLatch latch = new CountDownLatch(numberOfThreads);
		for (int i = 0; i < numberOfThreads; i++) {
			service.submit(() -> {
				logger.debug(Instant.now() + ", TID=" + Thread.currentThread().getId());
					try {
						BikeDTO bike = takeRandomBikeFromTheSystem();
						if (bike.getTaken()) {
							logger.debug(Instant.now() + ", TID=" + Thread.currentThread().getId() + ", RETURN: " + bike);
							//return
							BikeStandDTO stand = takeRandomStandFromTheSystem();
							if (stand.getParkedBikeId() != null) {
								//if stand is occupied return to station
								bike.setStationId(stand.getParentStationId());
								bike.setStandId(null);
							} else {
								//if stand is free return to stand
								bike.setStationId(stand.getParentStationId());
								bike.setStandId(stand.getStandId());
							}
							bike.setTaken(null);
							returnTheBikeToStationRequest(bike, stand.getParentStationId());
						} else {
							//rent
							logger.debug(Instant.now() + ", TID=" +Thread.currentThread().getId() + ", RENT: " + bike);
							bike.setTaken(null);
							rentTheBikeRequest(bike);
						}
					} catch (Exception e) {
						logger.warn(e.getMessage());
					}
					latch.countDown();
			});
		}
		latch.await();
		service.shutdown();
		TimeUnit.SECONDS.sleep(2);
		int availableBikesAfter = countAllAvailableBikes(getAllStationsRequest());
		int allBikesAfter = bikeStationService.getNumberOfAllBikes();
		int takenBikesAfter = bikeStationService.getNumberOfTakenBikes();

		logger.info("availableBikesAfter: " + availableBikesAfter);
		logger.info("allBikesAfter: " + allBikesAfter);
		logger.info("takenBikesAfter: " + takenBikesAfter);

		if (logger.isDebugEnabled()) {
			debugAllStationsRequest();
			debugBikesInfo();
			debugStationsBikesInfo();
		}

		assertEquals(allBikesBefore, allBikesAfter);
		assertEquals(allBikesAfter, takenBikesAfter + availableBikesAfter);

	}

	private void debugBikesInfo() {
		logger.debug(" ---- ALL BIKES -----");
		int i = 1;
		for (Bike bike : bikeStationService.getAllBikes().values()) {
			logger.debug("Bike " + (i++) + ": " + bike.toBikeDTO());
		}
	}

	private void debugStationsBikesInfo() {
			int i=1;
			for(BikeStation station : bikeStationService.getAllStations().values()) {
				logger.debug("Station " + (i++) + ": " + station.getId());
				int j=1;
				for(Bike bike : station.getAvailableBikes().values()) {
					logger.debug("Bike " + (j++) + ": " + bike.toBikeDTO());
				}
				logger.debug("---general stand--------");
				int k=1;
				for(Bike bike : station.getGeneralStationStand().values()) {
					logger.debug("Bike " + (k++) + ": " + bike.toBikeDTO());
				}
			}
	}

	private void debugAllStationsRequest() throws Exception {
		MvcResult result = mockMvc.perform(get("/bike-stations")
				.contentType(MediaType.APPLICATION_JSON)).andReturn();
		logger.debug(result.getResponse().getContentAsString());
	}
}
